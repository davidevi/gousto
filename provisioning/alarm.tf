/*
  This creates a CloudWatch alarm and an SNS topic.

  Terraform doesn't allow for e-mail subscriptions to be automatically
  created, so that step would either have to be done manually or a different
  SNS topic could be provided in as a parameter.
*/

resource "aws_sns_topic" "alarm" {
  name = "clojure-collector-alarms-topic"

  delivery_policy = <<EOF
{
  "http": {
    "defaultHealthyRetryPolicy": {
      "minDelayTarget": 20,
      "maxDelayTarget": 20,
      "numRetries": 3,
      "numMaxDelayRetries": 0,
      "numNoDelayRetries": 0,
      "numMinDelayRetries": 0,
      "backoffFunction": "linear"
    },
    "disableSubscriptionOverrides": false,
    "defaultThrottlePolicy": {
      "maxReceivesPerSecond": 1
    }
  }
}
EOF
}

resource "aws_cloudwatch_metric_alarm" "clojure-collector-cpu-alarm" {
  alarm_name                = "clojure-collector-cpu-alarm"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  metric_name               = "CPUUtilization"
  namespace                 = "AWS/EC2"
  period                    = "60"
  evaluation_periods        = "10"
  threshold                 = "70"
  statistic                 = "Minimum"
  alarm_description         = "Alerts when Clojure Collector EC2 usage is over 70% for more than 10 minutes"
  alarm_actions             = ["${aws_sns_topic.alarm.arn}"]
  insufficient_data_actions = []

  dimensions = {
    AutoScalingGroupName = "${aws_autoscaling_group.clojure-collector-asg.name}"
  }
}
