/*
  This contains the LaunchConfiguration used by the AutoScalingGroup.

  The AMI ID for Ubuntu 18.04 Bionic is automatically determined for the
  configured AWS region, and then used as part of the lauch configuration.

  A user data file (init.bash) is used to configure the instances.
*/

data "aws_ami" "ubuntu" {
  most_recent = true

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-bionic-18.04-amd64-server-*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["099720109477"] # Canonical
}

resource "aws_launch_configuration" "clojure-collector-launch-config" {
  name_prefix       = "clojure-collector-"
  image_id          = "${data.aws_ami.ubuntu.id}"
  instance_type     = "${var.instance_size}"
  enable_monitoring = true
  key_name          = "${var.instance_keypair}"

  user_data = "${file("init.bash")}"

  security_groups = ["${aws_security_group.clojure-collector-asg-sg.id}"]

  lifecycle {
    create_before_destroy = true
  }
}
