/*
  Contains two security groups:
  - One for the ELB that allows any ingress TCP on port 80
  - One for the ASG that allows ingress from the ELB's SG to port 8080
*/

resource "aws_security_group" "clojure-collector-elb-sg" {
  name        = "clojure-collector-elb-sg"
  description = "Allow public HTTP traffic to ELB"

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_security_group" "clojure-collector-asg-sg" {
  name        = "clojure-collector-asg-sg"
  description = "Allow internal traffic from ELB"

  ingress {
    from_port       = 8080
    to_port         = 8080
    protocol        = "tcp"
    security_groups = ["${aws_security_group.clojure-collector-elb-sg.id}"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}
