/*
  Contains the AutoScalingGroup and AutoScalingPolicy.

  The AutoScalingGroup spans two the `a` and `b` availability zones of the
  configured region. Health checks are performed via the ELB.

  The AutoScalingPolicy dictates that average CPU utilisation of the ASG
  should stay at around 70%.
*/

resource "aws_autoscaling_group" "clojure-collector-asg" {
  name               = "clojure-collector"
  availability_zones = ["${var.aws_region}a", "${var.aws_region}b"]
  min_size           = "${var.min_size}"
  max_size           = "${var.max_size}"

  health_check_grace_period = 400
  health_check_type         = "ELB"

  launch_configuration = "${aws_launch_configuration.clojure-collector-launch-config.name}"

  lifecycle {
    create_before_destroy = true
  }

  enabled_metrics = [
    "GroupDesiredCapacity",
    "GroupInServiceInstances",
    "GroupMaxSize",
    "GroupMinSize",
    "GroupPendingInstances",
    "GroupStandbyInstances",
    "GroupTerminatingInstances",
    "GroupTotalInstances",
  ]
}

resource "aws_autoscaling_policy" "clojure-collector-scaling-policy" {
  name                   = "clojure-collector"
  autoscaling_group_name = "${aws_autoscaling_group.clojure-collector-asg.name}"
  policy_type            = "TargetTrackingScaling"
  adjustment_type        = "ChangeInCapacity"

  target_tracking_configuration {
    predefined_metric_specification {
      predefined_metric_type = "ASGAverageCPUUtilization"
    }

    target_value = 70
  }
}
