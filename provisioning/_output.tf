output "load_balancer_dns" {
  value = "${aws_elb.clojure-collector-elb.dns_name}"
}
