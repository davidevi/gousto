#!/bin/bash

# Installing & cloning dependencies
sudo apt-get update
sudo apt-get install -y \
    python-dev \
    libxml2-dev \
    libxslt-dev \
    python-pip \
    python-dev \
    libffi-dev \
    libssl-dev \
    libxslt1-dev \
    libjpeg8-dev \
    zlib1g-dev \
    git \
    make \
    python-pip

sudo pip install setuptools --upgrade
sudo pip install ansible
git clone https://davidevi@bitbucket.org/davidevi/gousto.git

# Cloning playbook and installing requirements
cd gousto
mkdir configuring/roles
make requirements

# Running playbook
make playbook
