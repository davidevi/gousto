variable "aws_access_key" {}
variable "aws_secret_key" {}

variable "aws_region" {
  default = "us-east-1"
}

variable "instance_size" {
  default = "t2.micro"
}

variable "instance_keypair" {}

variable "min_size" {
  default = "1"
}

variable "max_size" {
  default = "2"
}
