/*
  Creates an AWS Elastic Load Balancer that listens on port 80 and balances
  load between the instances of the ASG.
*/

resource "aws_elb" "clojure-collector-elb" {
  name               = "clojure-collector-elb"
  availability_zones = ["${var.aws_region}a", "${var.aws_region}b"]

  listener {
    instance_port     = 8080
    instance_protocol = "http"
    lb_port           = 80
    lb_protocol       = "http"
  }

  health_check {
    healthy_threshold   = 2
    unhealthy_threshold = 2
    timeout             = 3
    target              = "HTTP:8080/"
    interval            = 30
  }

  cross_zone_load_balancing   = true
  idle_timeout                = 400
  connection_draining         = true
  connection_draining_timeout = 400

  security_groups = ["${aws_security_group.clojure-collector-elb-sg.id}"]

  tags = {
    Name = "clojure-collector-elb"
  }
}

resource "aws_autoscaling_attachment" "clojure-collector-elb-asg-attachment" {
  autoscaling_group_name = "${aws_autoscaling_group.clojure-collector-asg.id}"
  elb                    = "${aws_elb.clojure-collector-elb.id}"
}
