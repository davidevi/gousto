# Clojure Collector Stack

Deploys a multi-region, autoscalable, load-balanced stack in AWS that hosts the Clojure Collector.

This is the instructions document. See the [solution document](SOLUTION.md) for information on design and architecture.


### Requirements

- `make`
- [Terraform](https://www.terraform.io/downloads.html) (Tested with version 0.11)

## Usage

Export your AWS credentials as environment variables for Terraform:
```
export TF_VAR_aws_access_key=<...>
export TF_VAR_aws_secret_key=<...>
```

Optionally, specify the region (it defaults to us-east-1 as it is the cheapest)
```
export TF_VAR_aws_region=<...>
```

Export the name of the Key Pair you would like to use with the stack:
```
export TF_VAR_instance_keypair=<...>
```

Spin up the stack:
```
make demo
```

The output of the previous command should contain the DNS name of the ELB:
```
Apply complete! Resources: 9 added, 0 changed, 0 destroyed.

Outputs:

load_balancer_dns = clojure-collector-elb-119183924.us-east-1.elb.amazonaws.com
```

Wait for the DNS to propagate and the application to deploy, then test if the application is deployed correctly using `curl`:
```
curl -v clojure-collector-elb-119183924.us-east-1.elb.amazonaws.com/clojure-collector-1.1.0-standalone/i
```

In order to destroy the stack:
```
> make destroy
# Type 'yes' and press ENTER when prompted
```
