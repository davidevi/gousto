.ONESHELL:
SHELL = /bin/bash

# == Provisioning

init:
	cd provisioning && terraform init;

validate:
	cd provisioning && terraform validate;

apply: validate
	cd provisioning && terraform apply;

destroy:
	cd provisioning && terraform destroy;

# == Configuring

requirements:
	@mkdir configuring/roles || true
	cd configuring && \
		ansible-galaxy install -c --roles-path roles -r requirements.yml

playbook:
	cd configuring && \
		ansible-playbook -K clojure-collector.yml

# == Demo

demo:
	cd provisioning && \
		terraform init && \
		terraform apply -auto-approve;
