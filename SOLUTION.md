# Clojure Collector Stack


# Provisioning

**Note:** For convenience, detailed comments have been added at the top of the Terraform source files.

The infrastructure consists of:
- An [AutoScalingGroup](provisioning/scaling.tf)
    * Configured with a [LaunchConfiguration](provisioning/instances.tf)
- An [ElasticLoadBalancer](provisioning/load_balancer.tf)
- [Security Groups](provisioning/network.tf) are used to secure and facilitate traffic to and within the stack
- A [CloudWatch alarm](provisioning/alarm.tf)

The deployment has been made highly available by ensuring that the AutoScalingGroup, LaunchConfiguration, and ElasticLoadBalancer all use more than one Availability Zone. To be more precise, they will use zones `a` and `b` of the configured region (The region is configured using the `aws_region` variable).

The minimum and maximum number of instances in the ASG is configurable (variables `min_size`and `max_size`) and defaults to a minimum of 1 and a maximum of 2. The default instance size is `t2.micro` soley for costs reasons during development.

The stack adjusts to increases in load by scaling using the ASG, and by balancing the load using the ELB.

As per requirements, a CloudWatch alarm has been set up that triggers after 10 minutes of having the CPU usage over 70%.

# Configuration

As part of the LaunchConfiguration, the [init.bash](provisioning/init.bash) user data script is loaded. The sole purpose of this script is to run the Ansible playbook found in `/configuring`.

This is achieved by:
- Installing the necessary dependencies
- Cloning this project (in a non-tech-challenge scenario the repository would not be public, would perhaps be an S3 bucket and access would be configured using instance roles)
- Installing the requirements
- Running the playbook

[The playbook](configuring/clojure-collector.yml) runs locally on the target machine and installs Tomcat using a role found on Ansible galaxy.

Afterwards it downloads the Clojure Collector WAR file and places it in the Tomcat's `webapps`.

### Assumptions made
- It is assumed that the clojure collector WAR is located somewhere reliable and that the download URL will not go down.

### Potential Improvements
- Increasing spin-up speed by using an AMI with pre-installed Tomcat server.
- Ideally the Ansible requirements would be reviewed in advance to ensure there are no major security concerns.
- Would normally clone the Ansible requirements into a private repo as opposted to relying on Ansible Galaxy.
- The Ansible playbooks (`/configuring/*`) could be uploaded to S3 as part of a CI process instead of relying on the repository being available.

